import 'package:flutter/material.dart';

class Feature1 extends StatefulWidget {
  const Feature1({Key? key}) : super(key: key);

  @override
  _Feature1State createState() => _Feature1State();
}

class _Feature1State extends State<Feature1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("Blackboard - Feature 1"),
        ),
        body: const Center(child: Text("Blackboard Feature Coming Soon")));
  }
}
